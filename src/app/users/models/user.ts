export class User {
  id: number;
  email: string;
  name: string;
  fullName: string;
  firstName: string;
  lastName: string;
  userSectionPerms: Object;
}
