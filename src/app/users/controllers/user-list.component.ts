import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {UserService} from '../services/user.service';
import {Observable} from 'rxjs/Observable';
import {StateLoaderService} from '../../common/services/state-loader.service';

import {User} from '../models/user'
import {Constants} from '../../common/constants'

@Component({
    selector: 'users',
    templateUrl: '../views/user-list.component.html',
    styleUrls: ['../styles/user.component.scss']
})
export class UserListComponent implements OnInit {

    users:User[];
    loading:Boolean;
    tableData;
    rowActions:Object;
    errorMessage:Object;

    constructor(private stateLoaderService:StateLoaderService,
                private userService:UserService,
                private router:Router) {
        this.tableData = {
            columns: [
                {
                    key: 'id',
                    name: 'ID'
                },
                {
                    key: 'fullName',
                    name: 'User Name'
                },
                {
                    key: 'publisher',
                    name: 'Publisher Name'
                },
                {
                    key: 'email',
                    name: 'Email'
                },
                {
                    key: 'lastSignInAt',
                    name: 'Last Sign In',
                    pipe: 'date'
                }
            ],
            rows: [],
            orderBy: 'id',
            order: Constants.ORDER.DESCENDING,
            title: "Users"
        };

        this.rowActions = [
            {
                name: 'Show',
                icon: 'remove_red_eye',
                routerLink: 'user/',
            },
            {
                name: 'Become',
                icon: 'open_in_new',
                onClick: this.becomeUser,
            }];
    }

    /***
     * Returns the list of Users
     */
    getUsers():void {
        this.stateLoaderService.show();
        this.userService.all()
            .subscribe(
                users => this.tableData.rows = users,
                error => this.errorMessage = <any>error,
                () => this.stateLoaderService.hide());
    }

    /***
     * Stub
     */
    becomeUser(row):void {
        console.log("becomeSection stub")
    }

    /***
     * Filters results items for searchPhrase
     * @param items
     * @param searchPhrase
     * @returns {any}
     */
    filter(items:Array<User>, searchPhrase:string):Array<User> {
        return this.userService.filter(items, searchPhrase);
    }

    /***
     * Filters results items for searchPhrase
     * @param items
     * @param searchPhrase
     * @returns {any}
     */
    search(searchPhrase:string):Observable<User[]> {
        return this.userService.search(searchPhrase);
    }

    ngOnInit():void {
        this.loading = true;
        this.getUsers();
    }

}
