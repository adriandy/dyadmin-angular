import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {MdDialog, MdDialogRef} from '@angular/material';
import {BrowserModule, Title}  from '@angular/platform-browser';

/***********
 * Models
 ***********/
import {User} from '../models/user'
/***********
 * Services
 ***********/
import {UserService} from '../services/user.service'
import {StateLoaderService} from '../../common/services/state-loader.service'
/************
 * Components
 ************/
import {Constants} from '../../common/constants'


@Component({
    selector: 'users',
    templateUrl: '../views/user-view.component.html',
    styleUrls: ['../styles/user.component.scss']
})
export class UserViewComponent implements OnInit {

    userSectionPermsTableData;
    userSectionPermsTableRowActions:Object;
    usersTableData;
    usersTableRowActions:Object;
    loading:boolean;
    user:User = null;
    errorMessage:Object;

    constructor(private stateLoaderService:StateLoaderService,
                private userService:UserService,
                private router:Router,
                private activatedRoute:ActivatedRoute,
                private dialog:MdDialog,
                private titleService:Title) {


        this.userSectionPermsTableData = {
            columns: [
                {
                    key: 'sectionId',
                    name: 'Section'
                },
                {
                    key: 'section.name',
                    name: 'Section Name'
                },
                {
                    key: 'permission',
                    name: 'Permission'
                },
                {
                    key: 'codePermission',
                    name: 'Code Permission'
                },
                {
                    key: 'fpPermission',
                    name: 'FP Permission'
                }
            ],
            rows: [],
            orderBy: 'id',
            order: Constants.ORDER.DESCENDING,
            title: "Features"
        };

        this.userSectionPermsTableRowActions = [
            {
                name: 'Edit',
                icon: 'label',
                onClick: this.doSomething,
            }
        ];

    }

    /***
     * Returns a User by id
     * @param id
     */
    getUser(id:number):void {
        this.stateLoaderService.show();
        this.userService.load(id)
            .subscribe(
                user => {
                    this.user = user;
                    this.userSectionPermsTableData.rows = user.userSectionPerms;
                    this.titleService.setTitle(`View User - ${user.firstName} ${user.lastName}`)
                },
                error => this.errorMessage = <any>error,
                () => this.stateLoaderService.hide())
    }

    /***
     * Returns whether current user logged in is an Admin
     * @returns {boolean}
     */
    isCurrentUserAdmin():boolean {
        return this.userService.isAdmin;
    }

    /***
     * Init function
     */
    ngOnInit():void {
        this.loading = true;
        this.activatedRoute.params.subscribe((params:Params) => {
            let userId = Number(params['id']);
            this.getUser(userId);
            this.isCurrentUserAdmin();
            this.titleService.setTitle("View User");
        });
    }

    /***
     * Stub
     */
    doSomething():void {
        console.log('Do Something Here!');
    }

    /***
     * View Section
     */
    viewSection(row):void {
        this.router.navigate(["/sections", row.id]);
    }

}
