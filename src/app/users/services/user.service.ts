import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import '../../common/constants';
import * as _ from 'lodash';
//Models
import {User} from '../models/user';
import {Constants} from '../../common/constants';

@Injectable()
export class UserService {
    private USERS_URL = '/admin2/users';

    public isAdmin:boolean = false;

    constructor(private http:Http) {
    }

    static process(item) {
        return item;
    }

    private responsePostProcess(res:Response) {
        let body = res.json();
        if (body.length) {
            body = body.map(function (item) {
                return UserService.process(item);
            });
        }
        else {
            body = UserService.process(body);

        }
        return body || {};
    }

    private handleError(error:Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg:string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    /***
     * Returns list of all Users
     */
    all():Observable<User[]> {
        return this.http.get(this.USERS_URL)
            .map(this.responsePostProcess)
            .catch(this.handleError);
    }

    /***
     * Searches the server for a given item containing searchPhrase
     * @param searchPhrase
     * @returns {Maybe<T>|Promise<R>|any|Promise<ErrorObservable|T>|Promise<ErrorObservable>|Promise<T>}
     */
    search(searchPhrase:string):Observable<User[]> {
        return this.http.post(`${this.USERS_URL}/search`, {keyword: searchPhrase})
            .map(this.responsePostProcess)
            .catch(this.handleError);
    }

    /***
     * Receives a list of rows and filters them by searchPhrase
     * @param rows
     * @param searchPhrase
     * @returns {any}
     */
    //TODO: Add option to search for active sectionType like in publisher service
    filter(rows, searchPhrase) {
        return rows.filter(function (row) {
            return row.fullName.toLowerCase().includes(searchPhrase.toLowerCase()) ||
                row.id.toString().includes(searchPhrase.toLowerCase());
        })
    }


    /***
     * Loads content of a specific user by id
     * @param id
     * @returns {Maybe<T>|Promise<ErrorObservable>|Promise<ErrorObservable|T>|Promise<T>|Promise<R>|any}
     */
    load(id:number):Observable<User> {
        return this.http.get(`${this.USERS_URL}/${id}`)
            .map(this.responsePostProcess)
            .catch(this.handleError);
    }

    /***
     * Returns whether current user is an admin or not
     * @returns {Promise<ErrorObservable>|Promise<ErrorObservable|T>|any|Maybe<T>|Promise<R>}
     */
    isCurrentUserAdmin() {
        return this.http.get(`${this.USERS_URL}/is_admin`)
            .map(res=> {
                this.isAdmin = res.json();
            })
            .catch(this.handleError)
    }

}
