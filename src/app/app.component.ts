import {Component, OnInit} from '@angular/core';
import {MdIconRegistry} from '@angular/material';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import {DomSanitizer, Title} from '@angular/platform-browser';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/first';

import {UserService} from './users/services/user.service'
import {PublisherService} from './publishers/services/publisher.service'


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    // styleUrls: ['./../styles.scss']
})
export class AppComponent implements OnInit {

    constructor(iconRegistry:MdIconRegistry,
                sanitizer:DomSanitizer,
                private userService:UserService,
                private publisherService:PublisherService,
                private router:Router,
                private activatedRoute:ActivatedRoute,
                private titleService:Title) {
        iconRegistry.addSvgIcon('logo', sanitizer.bypassSecurityTrustResourceUrl('assets/images/logo.svg'));
        iconRegistry.addSvgIcon('robot', sanitizer.bypassSecurityTrustResourceUrl('assets/images/robot.svg'));
    }

    /***
     * Stub
     */
    logout():void {
        console.log('logout stub');
    }

    /***
     * Stub
     */
    goToProfile():void {
        console.log('profile stub');
    }

    ngOnInit():void {
        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .map(() => this.activatedRoute)
            .map(route => {
                while (route.firstChild) route = route.firstChild;
                return route;
            })
            .subscribe((event) => {
                this.titleService.setTitle(event.data['_value'].title);
            });

        this.userService.isCurrentUserAdmin()
            .subscribe(res=> {
                //Empty subscribe
            });

        let observableArray = [this.publisherService.getCountries(),this.publisherService.getCurrencies(),this.publisherService.getWhiteLabels()]
        Observable.forkJoin(...observableArray).subscribe(results => {
        });

    }
}