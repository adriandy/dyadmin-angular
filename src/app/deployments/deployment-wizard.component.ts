import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-deployments',
  templateUrl: './deployment-wizard.component.html',
  styleUrls: ['./deployment-wizard.component.css']
})
export class DeploymentWizard implements OnInit {

  tiers = [
    {value: 't1', text: 'Tier1'},
    {value: 't2', text: 'Tier2'},
    {value: 't3', text: 'Tier3'}
  ];
  
  constructor() { }

  ngOnInit() {
  }

}
