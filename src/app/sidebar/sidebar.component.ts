import {Component, OnInit} from '@angular/core';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import {StateLoaderService} from '../common/services/state-loader.service';

@Component({
    selector: 'sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit{
    adminMenuOptions:{ title:string, icon:string, routerLink:string, entity:string }[] = [
        {title: 'Publishers', icon: 'account_balance', routerLink: 'publishers', entity: 'publisher'},
        {title: 'Sections', icon: 'folder', routerLink: 'sections', entity: 'section'},
        {title: 'Users', icon: 'supervisor_account', routerLink: 'users', entity: 'user'},
        {title: 'Features', icon: 'extension', routerLink: 'features', entity: 'feature'},
        {title: 'Deployment', icon: 'settings_applications', routerLink: 'deployment', entity: 'deployment'},
        {title: 'Admin Users', icon: 'security', routerLink: 'admins', entity: ''},
        // {title: 'HBASE Printer', icon: 'print', routerLink: 'hbase', entity: ''},
        {title: 'Resque Jobs', icon: 'list', routerLink: 'resque', entity: 'resque'},
    ];

    activeMenuOption:number = 0;
    //Indicates the current state's entity => determines what menu option to highlight
    currentStateEntity;

    constructor(private stateLoaderservice:StateLoaderService,
                private router:Router,
                private activatedRoute: ActivatedRoute) {

    }

    setActive(index):void {
        this.activeMenuOption = index;
    }

    getLoadingStatus() {
        return this.stateLoaderservice.getLoadingStatus();
    }

    ngOnInit():void {
        this.router.events
            .filter(event => event instanceof NavigationEnd)
            .map(() => this.activatedRoute)
            .map(route => {
                while (route.firstChild) route = route.firstChild;
                return route;
            })
            .subscribe((event) => {
                this.currentStateEntity =event.data['_value'].entity;
            });
    }
}
