import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {PublisherService} from '../services/publisher.service';
import {StateLoaderService} from '../../common/services/state-loader.service';
import {Observable} from 'rxjs/Observable';

import {Publisher} from '../models/publisher';
import {Constants} from '../../common/constants';

@Component({
    selector: 'publishers',
    templateUrl: '../views/publisher-list.component.html',
    styleUrls: ['../styles/publisher.component.scss']
})
export class PublisherListComponent implements OnInit {

    publishers:Publisher[];
    loading:Boolean;
    tableData;
    rowActions:Object;
    errorMessage:Object;

    constructor(private stateLoaderService:StateLoaderService,
                private publisherService:PublisherService,
                private router:Router) {
        this.tableData = {
            columns: [
                {
                    key: 'id',
                    name: 'ID'
                },
                {
                    key: 'name',
                    name: 'Publisher Name'
                },
                {
                    key: 'country',
                    name: 'Country'
                },
                {
                    key: 'customerVertical',
                    name: 'Vertical',
                    pipe: 'toCustomerVertical'
                },
                {
                    key: 'accountType',
                    name: 'Type',
                    pipe: 'toAccountType'
                },
                {
                    key: 'active',
                    name: 'Active'
                },
                {
                    key: 'createdAt',
                    name: 'Created',
                    pipe: 'date'
                }
            ],
            rows: [],
            orderBy: 'id',
            order: Constants.ORDER.DESCENDING,
            title: 'Publishers'
        };

        this.rowActions = [
            {
                name: 'Show',
                icon: 'remove_red_eye',
                routerLink: 'publisher/',
            },
            {
                name: 'Become',
                icon: 'open_in_new',
                onClick: (row) => {this.becomePublisher(row)},
            }];
    }

    /***
     * Returns the list of Publishers
     */
    getPublishers():void {
        this.stateLoaderService.show();
        this.publisherService.all()
            .subscribe(
                publishers => this.tableData.rows = publishers,
                error => this.errorMessage = <any>error,
                () => this.stateLoaderService.hide());
    }


    /***
     * Stub
     */
    becomePublisher(row):void {
        this.publisherService.become(row.id);
    }

    /***
     * Filters results items for searchPhrase
     * @param items
     * @param searchPhrase
     * @returns {any}
     */
    filter(items:Array<Publisher>, searchPhrase:string):Array<Publisher> {
        return this.publisherService.filter(items, searchPhrase);
    }

    /***
     * Filters results items for searchPhrase
     * @param items
     * @param searchPhrase
     * @returns {any}
     */
    search(searchPhrase:string):Observable<Publisher[]> {
        return this.publisherService.search(searchPhrase);
    }

    ngOnInit():void {
        this.loading = true;
        this.getPublishers();
    }

}
