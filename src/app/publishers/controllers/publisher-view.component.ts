import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {MdDialog, MdDialogRef, MdDialogConfig} from '@angular/material';
import {Title}  from '@angular/platform-browser';
import {ToastsManager} from 'ng2-toastr/ng2-toastr';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/first';
import * as _ from 'lodash';

/***********
 * Models
 ***********/
import {Publisher} from '../models/publisher'
/***********
 * Services
 ***********/
import {PublisherService} from '../services/publisher.service'
import {UserService} from './../../users/services/user.service'
import {SectionService} from './../../sections/services/section.service'
import {StateLoaderService} from '../../common/services/state-loader.service'
/************
 * Components
 ************/
import {Constants} from '../../common/constants'
import {ConfirmationDialog} from '../../common/components/confirmation-dialog/confirmation-dialog'


@Component({
    selector: 'publishers',
    templateUrl: '../views/publisher-view.component.html',
    styleUrls: ['../styles/publisher.component.scss']
})
export class PublisherViewComponent implements OnInit {

    sectionTableData;
    sectionTableRowActions:Object;
    usersTableData;
    usersTableRowActions:Object;
    loading:boolean;
    publisher:Publisher = null;
    errorMessage:Object;
    currencies:Object = [];
    countries:Object = [];
    whiteLabels:Object = [];
    accountTypes = [
        {value: null, text: ''},
        {value: Constants.ACCOUNT_TYPES.TEST, text: 'Test'},
        {value: Constants.ACCOUNT_TYPES.DEMO, text: 'Demo'},
        {value: Constants.ACCOUNT_TYPES.ACTIVE, text: 'Active'},
        {value: Constants.ACCOUNT_TYPES.INACTIVE, text: 'Inactive'}
    ];
    customerVerticals = [
        {value: null, text: ''},
        {value: Constants.CUSTOMER_VERTICALS.ECOMMERCE, text: 'Ecommerce'},
        {value: Constants.CUSTOMER_VERTICALS.PUBLISHER, text: 'Publisher'},
        {value: Constants.CUSTOMER_VERTICALS.GAMING, text: 'Gaming'},
        {value: Constants.CUSTOMER_VERTICALS.OTHER, text: 'Other'}
    ];
    booleanOptions = [
        {value: false, text: 'false'},
        {value: true, text: 'true'},
    ];

    constructor(private stateLoaderService:StateLoaderService,
                private publisherService:PublisherService,
                private sectionService:SectionService,
                private userService:UserService,
                private router:Router,
                private activatedRoute:ActivatedRoute,
                private dialog:MdDialog,
                private titleService:Title,
                public toastr:ToastsManager,
                vcr:ViewContainerRef) {

        //Init the toaster
        this.toastr.setRootViewContainerRef(vcr);

        this.sectionTableData = {
            columns: [
                {
                    key: 'id',
                    name: 'ID'
                },
                {
                    key: 'name',
                    name: 'Name'
                },
                {
                    key: 'platform',
                    name: 'Platform'
                },
                {
                    key: 'active',
                    name: 'Active'
                },
                {
                    key: 'staticApiVer',
                    name: 'Script Version'
                },
            ],
            rows: [],
            orderBy: 'id',
            order: Constants.ORDER.DESCENDING,
            title: "Sections"
        };

        this.usersTableRowActions = [
            {
                name: 'Show',
                icon: 'remove_red_eye',
                onClick: (row) => {
                    this.viewUser(row)
                },
            },
            {
                name: 'Become',
                icon: 'open_in_new',
                onClick: this.redeploy,
            }
        ];


        this.usersTableData = {
            columns: [
                {
                    key: 'id',
                    name: 'ID'
                },
                {
                    key: 'fullName',
                    name: 'Name'
                },
                {
                    key: 'email',
                    name: 'Email'
                }
            ],
            rows: [],
            orderBy: 'id',
            order: Constants.ORDER.DESCENDING,
            title: 'Admin Users'
        };

        this.sectionTableRowActions = [
            {
                name: 'Show',
                icon: 'remove_red_eye',
                onClick: (row) => {
                    this.viewSection(row)
                },
            },
            {
                name: 'Become',
                icon: 'open_in_new',
                onClick: this.doSomething,
            }
        ];
    }


    doSomething() {
        console.log('do something');
    }

    /***
     * Returns a Publisher by id
     * @param id
     */
    getPublisher(id:number):void {
        this.stateLoaderService.show();
        this.publisherService.load(id)
            .subscribe(
                publisher => {
                    this.publisher = publisher;
                    this.sectionTableData.rows = publisher.sections;
                    this.usersTableData.rows = publisher.users;
                    this.titleService.setTitle(`View Publisher - ${publisher.name}`)
                },
                error => this.errorMessage = <any>error,
                () => this.stateLoaderService.hide())
    }

    /***
     * Returns whether current user logged in is an Admin
     * @returns {boolean}
     */
    isCurrentUserAdmin():boolean {
        return this.userService.isAdmin;
    }


    /***
     * Deactivates a publisher
     */
    deactivatePublisher():void {
        this.stateLoaderService.show();
        this.publisherService.deactivate(this.publisher.id)
            .subscribe(
                publisher => {
                    this.publisher = publisher;
                    this.sectionTableData.rows = publisher.sections;
                    this.usersTableData.rows = publisher.users;
                    this.toastr.success(`Publisher was deactivated successfully.`, 'Success');
                },
                error => this.toastr.error(`${error}`, 'Error'),
                () => this.stateLoaderService.hide())
    }

    // openDialog(errorMsg: string) {
    //   let dialogRef = this.dialog.open(AlertDialog);
    //   let instance = dialogRef.componentInstance;
    //   instance.message = errorMsg;
    //   instance.type = Constants.DIALOG.WARNING;
    // }

    /***
     * Init function
     */
    ngOnInit():void {
        this.loading = true;
        this.activatedRoute.params.subscribe((params:Params) => {
            let publisherId = Number(params['id']);
            this.getPublisher(publisherId);
            if (this.publisherService.countries && this.publisherService.currencies && this.publisherService.whiteLabels) {
                this.countries = this.publisherService.countries;
                this.currencies = this.publisherService.currencies;
                this.whiteLabels = this.publisherService.whiteLabels;
            }
            else {
                this.stateLoaderService.show();
                let observableArray = [this.publisherService.getCountries(), this.publisherService.getCurrencies(), this.publisherService.getWhiteLabels()]
                Observable.forkJoin(...observableArray).subscribe(results => {
                    this.countries = results[0];
                    this.currencies = results[1];
                    this.whiteLabels = results[2];
                    this.stateLoaderService.hide();
                });
            }

            this.isCurrentUserAdmin();
            this.titleService.setTitle("View Publisher");
        });
    }

    /***
     * Updates publisher attributes
     * @param obj
     */
    updatePublisher(obj):void {
        this.stateLoaderService.show();
        let key = Object.keys(obj)[0];
        this.publisherService.update(this.publisher.id, {publisher: obj})
            .subscribe(section=> {
                    this.toastr.success(`${key} was updated`, 'Success');
                },
                error => {
                    this.toastr.error(`${key} could not be updated`, 'Problem');
                },
                () => this.stateLoaderService.hide());
    }

    /***
     * Redeploy all publisher sections
     */
    redeploy():void {
        let sections = _.get(this, 'publisher.sections', []);
        let config = new MdDialogConfig();
        let dialogRef = this.dialog.open(ConfirmationDialog, config);
        dialogRef.componentInstance.title = `Redeploy Scripts?`;
        dialogRef.componentInstance.message = `This will redeploy scripts for the ${sections.length} sections associated with this Publisher. Are you sure you want to continue?`;
        dialogRef.afterClosed().subscribe(res => {
            if (res.redeploy) {
                let sections = _.map(this.publisher.sections, 'id');
                this.sectionService.redeploy(sections, res.invalidate)
                    .subscribe(res=> {
                            this.toastr.success(`${res}`, 'Success');
                        },
                        error => {
                            this.toastr.error(`${error}`, 'Problem');
                        },
                        () => this.stateLoaderService.hide());
            }
        });
    }


    /***
     * View Section
     */
    viewSection(row):void {
        this.router.navigate(["/sections", row.id]);
    }

    /***
     * View User
     */
    viewUser(row):void {
        this.router.navigate(["/users", row.id]);
    }

    /***
     * Becomes current publisher
     */
    become():void {
        this.publisherService.become(this.publisher.id);
    }

}