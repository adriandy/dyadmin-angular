import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import '../../common/constants';

//Models
import {Publisher} from '../models/publisher';
import {Constants} from '../../common/constants';

@Injectable()
export class PublisherService {
    private PUBLISHERS_URL = '/admin2/publishers';
    public countries: Array<Object> = null;
    public currencies: Array<Object> = null;
    public whiteLabels: Array<Object> = null;

    constructor(private http: Http) {
    }

    static process (item){
        let cVertical = item['customerVertical'];
        let accountType = item['accountType'];
        let createDate = new Date(item['createdAt']).toLocaleDateString();
        let active = item['active'] ? 'Active' : 'Inactive';

        switch (cVertical) {
            case Constants.CUSTOMER_VERTICALS.ECOMMERCE:
                cVertical = 'Ecommerce';
                break;
            case Constants.CUSTOMER_VERTICALS.PUBLISHER:
                cVertical = 'Publisher';
                break;
            case Constants.CUSTOMER_VERTICALS.GAMING:
                cVertical = 'Gaming';
                break;
            case Constants.CUSTOMER_VERTICALS.OTHER:
                cVertical = 'Other';
                break;
            default:
                cVertical = 'Unknown';
        }

        switch (accountType) {
            case Constants.ACCOUNT_TYPES.TEST:
                accountType = 'Test';
                break;
            case Constants.ACCOUNT_TYPES.DEMO:
                accountType = 'Demo';
                break;
            case Constants.ACCOUNT_TYPES.ACTIVE:
                accountType = 'Active';
                break;
            case Constants.ACCOUNT_TYPES.INACTIVE:
                accountType = 'Inactive';
                break;
            default:
                accountType = 'Unknown';
        }

        item['customerVertical'] = cVertical;
        item['accountType'] = accountType;
        item['createdAt'] = createDate;
        item['active'] = active;
        return item;
    }

    private responsePostProcess(res:Response) {
        let body = res.json();
        // if (body.length) {
        //     body = body.map(function (item) {
        //         return PublisherService.process(item);
        //     });
        // }
        // else {
        //     body = PublisherService.process(body);
        //
        // }
        return body || {};
    }

    private static returnResponse(res:Response) {
        return res.json() || {};
    }

    private static handleError(error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    /***
     * Returns list of all Publishers
     */
    all(): Observable<Publisher[]> {
        return this.http.get(this.PUBLISHERS_URL)
            .map(this.responsePostProcess)
            .catch(PublisherService.handleError);
    }

    /***
     * Searches the server for a given item containing searchPhrase
     * @param searchPhrase
     * @returns {Maybe<T>|Promise<R>|any|Promise<ErrorObservable|T>|Promise<ErrorObservable>|Promise<T>}
     */
    search(searchPhrase: string): Observable<Publisher[]>{
        return this.http.post(`${this.PUBLISHERS_URL}/search`, {keyword: searchPhrase})
            .map(this.responsePostProcess)
            .catch(PublisherService.handleError);
    }

    /***
     * Receives a list of rows and filters them by searchPhrase
     * @param rows
     * @param searchPhrase
     * @returns {any}
     */
    filter (rows:Array<Publisher>, searchPhrase:string){
        return rows.filter(function (row) {
            return row.name.toLowerCase().includes(searchPhrase.toLowerCase()) ||
                // row.customerVertical.toLowerCase().includes(searchPhrase.toLowerCase()) ||
                // row.accountType.toLowerCase().includes(searchPhrase.toLowerCase()) ||
                // row.active.toLowerCase().includes(searchPhrase.toLowerCase()) ||
                row.id.toString().includes(searchPhrase.toLowerCase());
        })
    }

    /***
     * Loads content of a specific publisher by id
     * @param id
     * @returns {Maybe<T>|Promise<ErrorObservable>|Promise<ErrorObservable|T>|Promise<T>|Promise<R>|any}
     */
    load(id: number): Observable<Publisher> {
        return this.http.get(`${this.PUBLISHERS_URL}/${id}`)
            .map(this.responsePostProcess)
            .catch(PublisherService.handleError);
    }

    /***
     * Deactivates a publisher
     * @param id
     * @returns {any|Promise<ErrorObservable|T>|Maybe<T>|Promise<ErrorObservable>|Promise<R>|Promise<T>}
     */
    deactivate(id: number){
        return this.http.post(`${this.PUBLISHERS_URL}/deactivate`, {id: id})
            .map(PublisherService.returnResponse)
            .catch(PublisherService.handleError);
    }

    /***
     * Become the current publisher
     * @param id
     */
    become(id: number) {
        window.open(`${this.PUBLISHERS_URL}/${id}/become`);
    }

    /***
     * Updates a given publisher by id with attributes in obj
     * Server should know how to interpret camelCase a\s snake_case
     * @param id
     * @param obj
     * @returns {Section}
     */
    update(id: number, obj: Object):Observable<Publisher> {
        return this.http.put(`${this.PUBLISHERS_URL}/${id}`, obj)
            .map(this.responsePostProcess)
            .catch(PublisherService.handleError);
    }

    /***
     *
     * Returns a list of currencies 
     * @returns {Promise<ErrorObservable>|Promise<ErrorObservable|T>|any|Maybe<T>|Promise<R>}
     */
    getCurrencies(){
        return this.http.get(`${this.PUBLISHERS_URL}/currencies`)
            .map(res=> {
                this.countries = res.json();
                return res.json();
            })
            .catch(PublisherService.handleError);
    }

    /***
     * Returns a list of countries
     * @returns {any|Promise<ErrorObservable|T>|Maybe<T>|Promise<ErrorObservable>|Promise<R>|Promise<T>}
     */
    getCountries(){
        return this.http.get(`${this.PUBLISHERS_URL}/countries`)
            .map(res=> {
                this.currencies = res.json();
                return res.json();

            })
            .catch(PublisherService.handleError);
    }
    
    
    getWhiteLabels(){
        return this.http.get(`${this.PUBLISHERS_URL}/white_labels`)
            .map(res=> {
                this.whiteLabels = res.json();
                return res.json();
            })
            .catch(PublisherService.handleError);
    }

}
