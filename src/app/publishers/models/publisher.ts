export class Publisher {
  id: number;
  name: string;
  type: string;
  revShare: number;
  vertical: string;
  currency: string;
  address1: string;
  city: string;
  country: string;
  phone: string;
  fax: string;
  active: boolean;
  override2Fa: boolean;
  created: string;
  overrideUntil: string;
  sections: any;
  users: any;
}
