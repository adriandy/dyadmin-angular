import {MaterialModule} from '@angular/material';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {InlineEditorModule} from 'ng2-inline-editor';

import 'hammerjs';

/***************
 * COMPONENTS
 ***************/
import {AppComponent} from './app.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {ControlTableComponent} from './common/components/control-table/control-table.component';
import {ConfirmationDialog} from './common/components/confirmation-dialog/confirmation-dialog';
import {AlertDialog} from './common/components/alert-dialog/alert-dialog.component';
import {PublisherListComponent} from './publishers/controllers/publisher-list.component';
import {PublisherViewComponent} from './publishers/controllers/publisher-view.component';
import {SectionListComponent} from './sections/controllers/section-list.component';
import {SectionViewComponent} from './sections/controllers/section-view.component';
import {UserListComponent} from './users/controllers/user-list.component';
import {UserViewComponent} from './users/controllers/user-view.component';
import {FeatureListComponent} from './features/controllers/feature-list.component';
import {ToastModule, ToastOptions} from 'ng2-toastr';
import {ToasterConfig} from './common/components/toaster-config/toaster-config';



/***************
 * SERVICES
 ***************/
import {PublisherService} from './publishers/services/publisher.service';
import {SectionService} from './sections/services/section.service';
import {UserService} from './users/services/user.service';
import {FeatureService} from './features/services/feature.service';
import {StateLoaderService} from './common/services/state-loader.service';

/***************
 * PIPES
 ***************/
import {ToCustomerVerticalPipe} from './common/pipes/to-customer-vertical.pipe';
import {ToAccountTypePipe} from './common/pipes/to-account-type.pipe';
import {TruncatePipe} from './common/pipes/truncate.pipe';
import {DeploymentWizard} from './deployments/deployment-wizard.component';

const appRoutes:Routes = [
    {
        path: 'publishers',
        component: PublisherListComponent,
        data: {title: 'Publishers List', entity: 'publisher'}
    },
    {
        path: 'publishers/:id',
        component: PublisherViewComponent,
        data: {title: 'Publishers View', entity: 'publisher'}
    },
    {
        path: 'sections',
        component: SectionListComponent,
        data: {title: 'Sections List', entity: 'section'}
    },
    {
        path: 'sections/:id',
        component: SectionViewComponent,
        data: {title: 'Sections View', entity: 'section'}
    },
    {
        path: 'users',
        component: UserListComponent,
        data: {title: 'Users List', entity: 'user'}
    },
    {
        path: 'users/:id',
        component: UserViewComponent,
        data: {title: 'Users View', entity: 'user'}
    },
    {
        path: 'features',
        component: FeatureListComponent,
        data: {title: 'Features List', entity: 'feature'}
    },
    {
        path: 'deployment',
        component: DeploymentWizard,
        data: {title: 'Deployments', entity: 'deployment'}
    },
    // { path: '**', component: PageNotFoundComponent }
];


@NgModule({
    declarations: [
        AppComponent,
        SidebarComponent,
        ToolbarComponent,
        PublisherListComponent,
        PublisherViewComponent,
        SectionListComponent,
        SectionViewComponent,
        UserListComponent,
        UserViewComponent,
        FeatureListComponent,
        ControlTableComponent,
        AlertDialog,
        ConfirmationDialog,
        ToCustomerVerticalPipe,
        ToAccountTypePipe,
        TruncatePipe,
        DeploymentWizard
    ],
    entryComponents: [
        AlertDialog,
        ConfirmationDialog
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ToastModule.forRoot(),
        RouterModule.forRoot(appRoutes),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        InlineEditorModule
    ],
    providers: [
        PublisherService,
        SectionService,
        UserService,
        FeatureService,
        StateLoaderService,
        {provide: ToastOptions, useClass: ToasterConfig}],
    bootstrap: [AppComponent]
})
export class AppModule {
}
