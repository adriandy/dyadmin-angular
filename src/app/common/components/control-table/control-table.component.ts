import {Component, OnInit, Input} from '@angular/core';
import {FormControl} from '@angular/forms';
import 'rxjs/add/operator/debounceTime';

import * as _ from 'lodash';

import {Constants} from '../../constants';
import {PublisherService} from '../../../publishers/services/publisher.service';
import {SectionService} from '../../../sections/services/section.service';
import {UserService} from '../../../users/services/user.service';

@Component({
    selector: 'control-table',
    templateUrl: './control-table.component.html',
    styleUrls: ['./control-table.component.scss'],
})
export class ControlTableComponent implements OnInit {
    @Input()
    tableData:any = {
        columns: [],
        rows: [],
        orderBy: null,
        order: Constants.ORDER.ASCENDING
    };

    @Input()
    rowActions:Object;

    @Input()
    padding:boolean = true;

    @Input()
    filterFunction;

    @Input()
    searchFunction;

    order:Object = Constants.ORDER;
    loading:boolean = false;
    searchPhrase = new FormControl();
    cachedRows = [];

    constructor(private publisherService:PublisherService,
                private sectionService:SectionService,
                private userService:UserService) {
    }

    /***
     * Order by stub
     */
    orderBy(key:string):void {
        this.tableData.orderBy = key;
        this.tableData.order = this.tableData.order === Constants.ORDER.ASCENDING ? Constants.ORDER.DESCENDING : Constants.ORDER.ASCENDING;
        this.tableData.rows = _.orderBy(this.tableData.rows, key, this.tableData.order);
    }

    /***
     * Search stub
     * @param event
     */
    onSearchType(event:any):void {
        return event.keyCode !== 13 ? null : this.search(event.target.value);
    }

    search(searchPhrase) {
        //Save cached version of the rows
        if (!this.cachedRows.length) {
            this.cachedRows = this.tableData.rows;
        }

        //Empty string - Return cached version of rows
        if (searchPhrase.trim().length === 0) {
            this.tableData.rows = this.cachedRows;
            return;
        }

        let context = this;

        //Filters the results locally
        if (this.filterFunction) {
            context.tableData.rows = this.filterFunction(context.cachedRows, searchPhrase);
        }

        //Initiates a search query
        if (this.searchFunction) {
            this.loading = true;
            this.searchFunction(searchPhrase)
                .subscribe(
                    results => {
                        _.merge(this.tableData.rows, results);
                    },
                    error => console.error(error),
                    ()=> this.loading = false)
        }

    }
    
    getRow(row, key){
        return _.get(row, `${key}`)
    }

    ngOnInit() {
        // debounce keystroke events
        this.searchPhrase.valueChanges
            .debounceTime(500)
            .subscribe(newValue => this.search(newValue));
    }
}
