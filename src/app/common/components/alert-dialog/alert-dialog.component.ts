import {Component, Input} from '@angular/core';
import {Constants} from '../../constants';

@Component({
  selector: 'alert-dialog',
  templateUrl: './alert-dialog.component.html',
})
export class AlertDialog{

  @Input()
  type: string = Constants.DIALOG.SUCCESS;

  @Input()
  message: string;
}
