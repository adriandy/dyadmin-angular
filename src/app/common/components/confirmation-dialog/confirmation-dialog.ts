import {Component} from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';

@Component({
    selector: 'confirmation-dialog',
    templateUrl: './confirmation-dialog.html',
    styleUrls: ['./confirmation-dialog.scss']
})
export class ConfirmationDialog {
    message = "Welcome";
    title = "Are you sure you want to redeploy?";
    invalidate:boolean = false;

    constructor(public dialogRef:MdDialogRef<ConfirmationDialog>) {

    }
}