import {Injectable} from '@angular/core';

@Injectable()
export class StateLoaderService {

    private loadingStatus: boolean = false;

    getLoadingStatus() {
        return this.loadingStatus;
    }

    setLoadingStatus(status: boolean = false) {
        this.loadingStatus = status;
    }

    show() {
        this.loadingStatus = true;
    }

    hide() {
        this.loadingStatus = false;
    }

}