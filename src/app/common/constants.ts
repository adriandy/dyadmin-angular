export class Constants {

    public static DIALOG = {
        ERROR: 'error',
        WARNING: 'warning',
        SUCCESS: 'success'
    };

    public static ORDER = {
        ASCENDING: 'asc',
        DESCENDING: 'desc'
    };

    public static CUSTOMER_VERTICALS = {
        ECOMMERCE: 0,
        PUBLISHER: 1,
        GAMING: 2,
        OTHER: 3
    };

    public static ACCOUNT_TYPES = {
        TEST: 0,
        DEMO: 1,
        ACTIVE: 2,
        INACTIVE: 3
    };
}
