import { Pipe, PipeTransform } from '@angular/core';
import {Constants} from '../constants';

@Pipe({name: 'toCustomerVertical'})
export class ToCustomerVerticalPipe implements PipeTransform {
    transform(value: number, args: string[]): any {

        let customerVertical;
        switch (value){
            case Constants.CUSTOMER_VERTICALS.ECOMMERCE:
                customerVertical = 'Ecommerce';
                break;
            case Constants.CUSTOMER_VERTICALS.PUBLISHER:
                customerVertical = 'Publisher';
                break;
            case Constants.CUSTOMER_VERTICALS.GAMING:
                customerVertical = 'Gaming';
                break;
            case Constants.CUSTOMER_VERTICALS.OTHER:
                customerVertical = 'Other';
                break;
            default:
                customerVertical = 'Undefined';
                break;
        }
        return customerVertical;
    }
}