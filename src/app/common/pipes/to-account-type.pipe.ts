import { Pipe, PipeTransform } from '@angular/core';
import {Constants} from '../constants';

@Pipe({name: 'toAccountType'})
export class ToAccountTypePipe implements PipeTransform {
    transform(value: number, args: string[]): any {

        let accountType;
        switch (value){
            case Constants.ACCOUNT_TYPES.TEST:
                accountType = 'Test';
                break;
            case Constants.ACCOUNT_TYPES.DEMO:
                accountType = 'Demo';
                break;
            case Constants.ACCOUNT_TYPES.ACTIVE:
                accountType = 'Active';
                break;
            case Constants.ACCOUNT_TYPES.INACTIVE:
                accountType = 'Inactive';
                break;
            default:
                accountType = 'Undefined';
                break;
        }
        return accountType;
    }
}