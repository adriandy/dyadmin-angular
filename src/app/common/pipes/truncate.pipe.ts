import { Pipe, PipeTransform } from '@angular/core';
import {Constants} from '../constants';

@Pipe({name: 'truncate'})
export class TruncatePipe implements PipeTransform {
    transform(value: string, args: string[]): any {
        let maxLength = 65;

        return value.length < maxLength ? value : `${value.substring(0,maxLength)}...`;
    }
}