import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import '../../common/constants';
import * as _ from 'lodash';

//Models
import {Feature} from '../models/feature';

@Injectable()
export class FeatureService {
    private FEATURES_URL = '/admin2/features';

    constructor(private http:Http) {
    }

    private static returnResponse(res:Response) {
        return res.json() || {};
    }

    private static handleError(error:Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg:string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }


    /***
     * Returns list of all Features
     */
    all():Observable<Feature[]> {
        return this.http.get(this.FEATURES_URL)
            .map(FeatureService.returnResponse)
            .catch(FeatureService.handleError);
    }

    /***
     * Receives a list of rows and filters them by searchPhrase
     * @param rows
     * @param searchPhrase
     * @returns {any}
     */
    filter(rows, searchPhrase) {
        return rows.filter(function (row) {
            let name = _.get(row, 'name', '');
            let feature = _.get(row, 'feature', '');
            return name.toLowerCase().includes(searchPhrase.toLowerCase()) || feature.toLowerCase().includes(searchPhrase.toLowerCase());
        })
    }
}
