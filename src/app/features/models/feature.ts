export class Feature {
  id: number;
  scriptVersion: number;
  feature: string;
  metaData: string;
}
