import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {FeatureService} from '../services/feature.service';
import {StateLoaderService} from '../../common/services/state-loader.service';

import {Feature} from '../models/feature';
import {Constants} from '../../common/constants';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'features',
    templateUrl: '../views/feature-list.component.html',
    styleUrls: ['../styles/feature.component.scss']
})
export class FeatureListComponent implements OnInit {

    features:Feature[];
    loading:Boolean;
    tableData;
    rowActions:Object;
    errorMessage:Object;

    constructor(private stateLoaderService:StateLoaderService,
                private featureService:FeatureService,
                private router:Router) {
        this.tableData = {
            columns: [
                {
                    key: 'feature',
                    name: 'Feature Name'
                },
                {
                    key: 'metaData',
                    name: 'Description',
                    pipe: 'truncate'
                },
                {
                    key: 'enabled',
                    name: 'Default State'
                },
                {
                    key: 'scriptVersion',
                    name: 'Script Version'
                },
                {
                    key: 'value',
                    name: 'Value'
                },
            ],
            rows: [],
            orderBy: 'name',
            order: Constants.ORDER.DESCENDING,
            title: 'Features'
        };

        this.rowActions = [
            {
                name: 'Show',
                icon: 'remove_red_eye',
                routerLink: 'feature/',
            }];
    }

    /***
     * Returns the list of Features
     */
    getFeatures():void {
        this.stateLoaderService.show();
        this.featureService.all()
            .subscribe(
                features => this.tableData.rows = features,
                error => this.errorMessage = <any>error,
                () => this.stateLoaderService.hide());
    }

    /***
     * Filters results items for searchPhrase
     * @param items
     * @param searchPhrase
     * @returns {any}
     */
    filter = (items:Array<Feature>, searchPhrase:string) => {
        return this.featureService.filter(items, searchPhrase)
    };

    ngOnInit():void {
        this.loading = true;
        this.getFeatures();
    }

}
