import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import '../../common/constants';
import * as _ from 'lodash';
//Models
import {Section} from '../models/section';
import {Constants} from '../../common/constants';

@Injectable()
export class SectionService {
    private SECTIONS_URL = '/admin2/sections';
    private API_VERSIONS_URL = '/api_versions';

    constructor(private http:Http) {
    }

    static process(item) {
        return item;
    }

    private responsePostProcess(res:Response) {
        let body = res.json();
        if (body.length) {
            body = body.map(function (item) {
                return SectionService.process(item);
            });
        }
        else {
            body = SectionService.process(body);

        }
        return body || {};
    }

    private static handleError(error:Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg:string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    /***
     * Returns list of all Sections
     */
    all():Observable<Section[]> {
        return this.http.get(this.SECTIONS_URL)
            .map(this.responsePostProcess)
            .catch(SectionService.handleError);
    }

    /***
     * Searches the server for a given item containing searchPhrase
     * @param searchPhrase
     * @returns {Maybe<T>|Promise<R>|any|Promise<ErrorObservable|T>|Promise<ErrorObservable>|Promise<T>}
     */
    search(searchPhrase:string):Observable<Section[]> {
        return this.http.post(`${this.SECTIONS_URL}/search`, {keyword: searchPhrase})
            .map(this.responsePostProcess)
            .catch(SectionService.handleError);
    }

    /***
     * Receives a list of rows and filters them by searchPhrase
     * @param rows
     * @param searchPhrase
     * @returns {any}
     */
    //TODO: Add option to search for active platforms like in section service
    filter(rows, searchPhrase) {
        return rows.filter(function (row) {
            return row.name.toLowerCase().includes(searchPhrase.toLowerCase()) ||
                row.platform.toLowerCase().includes(searchPhrase.toLowerCase()) ||
                row.staticApiVer.toString().includes(searchPhrase.toLowerCase()) ||
                row.id.toString().includes(searchPhrase.toLowerCase());
        })
    }


    /***
     * Loads content of a specific section by id
     * @param id
     * @returns {Maybe<T>|Promise<ErrorObservable>|Promise<ErrorObservable|T>|Promise<T>|Promise<R>|any}
     */
    load(id:number):Observable<Section> {
        return this.http.get(`${this.SECTIONS_URL}/${id}`)
            .map(this.responsePostProcess)
            .catch(SectionService.handleError);
    }

    become(id:number) {
        window.open(`${this.SECTIONS_URL}/${id}/become`);
    }

    redeploy(ids:Array<number>, invalidate:boolean = false):Observable<any> {
        let singleSection = ids.length === 1;
        let url = singleSection ? `${this.SECTIONS_URL}/${ids[0]}/redeploy` : `${this.SECTIONS_URL}/redeploy`;
        return this.http.post(url, {
            section_ids: singleSection ? null : ids,
            invalidation: invalidate,
        })
            .map(this.responsePostProcess)
            .catch(SectionService.handleError);
    }

    /***
     * Updates a given section by id with attributes in obj
     * Server should know how to interpret camelCase a\s snake_case
     * @param id
     * @param obj
     * @returns {Section}
     */
    update(id:number, obj:Object):Observable<Section> {
        return this.http.put(`${this.SECTIONS_URL}/${id}`, obj)
            .map(this.responsePostProcess)
            .catch(SectionService.handleError);
    }

}
