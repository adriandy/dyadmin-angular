export class Section {
  id: number;
  name: string;
  type: string;
  publisherName: string;
  active: boolean;
  segmentDefinition:string;
  depricatedVersion: number;
  scriptVersion:number;
  injection: boolean;
  url:string;
  customHomePagePattern:string;
  rawData: boolean;
  learn: boolean;
  predict: boolean;
  extendedAudience: boolean;
  overrideMFA: boolean;
  daysToArchive: number;
  rcomPersonalization: boolean;
  externalDataSource:string;
  features: any;
  users: any;
}
