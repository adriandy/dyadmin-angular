import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {MdDialog, MdDialogRef, MdDialogConfig, MdSnackBar, MdSnackBarConfig} from '@angular/material';
import {BrowserModule, Title}  from '@angular/platform-browser';
import * as _ from 'lodash';

/***********
 * Models
 ***********/
import {Section} from '../models/section'
import {Feature} from '../../features/models/feature'
/***********
 * Services
 ***********/
import {SectionService} from '../services/section.service'
import {FeatureService} from '../../features/services/feature.service'
import {UserService} from '../../users/services/user.service'
import {StateLoaderService} from '../../common/services/state-loader.service'
/************
 * Components
 ************/
import {Constants} from '../../common/constants'
import {ConfirmationDialog} from '../../common/components/confirmation-dialog/confirmation-dialog'

@Component({
    selector: 'sections',
    templateUrl: '../views/section-view.component.html',
    styleUrls: ['../styles/section.component.scss']
})
export class SectionViewComponent implements OnInit {

    featuresTableData;
    featuresTableRowActions:Object;
    loading:boolean;
    section:Section = null;
    errorMessage:Object;

    constructor(private stateLoaderService:StateLoaderService,
                private sectionService:SectionService,
                private  featureService:FeatureService,
                private  userService:UserService,
                private activatedRoute:ActivatedRoute,
                private dialog:MdDialog,
                private snackBar:MdSnackBar,
                private titleService:Title,
                private router:Router) {

        this.featuresTableData = {
            columns: [
                {
                    key: 'name',
                    name: 'Name'
                },
                // {
                //   key: 'metaData',
                //   name: 'Information'
                // },
                {
                    key: 'default',
                    name: 'Default'
                },
                {
                    key: 'currentState',
                    name: 'Current'
                }
            ],
            rows: [],
            orderBy: 'id',
            order: Constants.ORDER.DESCENDING,
            title: "Features"
        };

        this.featuresTableRowActions = [
            {
                name: 'Toggle',
                icon: 'radio_button_checked',
                onClick: this.doSomething,
            }
        ];
    }

    /***
     * Returns a Section by id
     * @param id
     */
    getSection(id:number):void {
        this.stateLoaderService.show();
        this.sectionService.load(id)
            .subscribe(
                section => {
                    this.section = section;
                    this.featuresTableData.rows = section.features;
                    this.titleService.setTitle(`View Section - ${section.name}`)
                },
                error => this.errorMessage = <any>error,
                () => this.stateLoaderService.hide())
    }

    /***
     * Init function
     */
    ngOnInit():void {
        this.loading = true;
        this.activatedRoute.params.subscribe((params:Params) => {
            let sectionId = Number(params['id']);
            this.getSection(sectionId);
        });
    }

    /***
     * Edit section
     */
    editSection():void {
        this.router.navigate(["/sections/edit", this.section.id]);
    }

    /***
     * Filters results items for searchPhrase
     * @param items
     * @param searchPhrase
     * @returns {any}
     */
    filterFeatures = (items:Array<Feature>, searchPhrase:string) => {
        return this.featureService.filter(items, searchPhrase)
    };

    /***
     * Redeploy all publisher sections
     */
    redeploy():void {
        let config = new MdDialogConfig();
        let dialogRef = this.dialog.open(ConfirmationDialog, config);
        dialogRef.componentInstance.title = `Redeploy Scripts?`;
        dialogRef.componentInstance.message = `This will redeploy scripts for the ${this.section.name}. Are you sure you want to continue?`;
        dialogRef.afterClosed().subscribe(res => {
            if (res.redeploy) {
                this.sectionService.redeploy([this.section.id], res.invalidate)
                    .subscribe(res=> {
                            console.log('res', res);
                        },
                        error => {
                            let snackBarRef = this.snackBar.open(error, '',
                                {
                                    duration: 2000,
                                });
                        },
                        () => this.stateLoaderService.hide());
            }
        });
    }

    /***
     * Returns whether current user logged in is an Admin
     * @returns {boolean}
     */
    isCurrentUserAdmin():boolean {
        return this.userService.isAdmin;
    }

    /***
     * Stub
     */
    doSomething():void {
        console.log('Do Something Here!');
    }

}
