import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {SectionService} from '../services/section.service';
import {StateLoaderService} from '../../common/services/state-loader.service';
import {Observable} from 'rxjs/Observable';
import * as _ from 'lodash';
import {Section} from '../models/section'
import {Constants} from '../../common/constants'
import {ToastsManager} from 'ng2-toastr/ng2-toastr';

@Component({
    selector: 'sections',
    templateUrl: '../views/section-list.component.html',
    styleUrls: ['../styles/section.component.scss']
})
export class SectionListComponent implements OnInit {

    sections:Section[];
    loading:Boolean;
    tableData;
    rowActions:Object;
    errorMessage:Object;

    constructor(private stateLoaderService:StateLoaderService,
                private sectionService:SectionService,
                private router:Router,
                private route:ActivatedRoute,
                public toastr:ToastsManager,
                vcr:ViewContainerRef) {

        //Init the toaster
        this.toastr.setRootViewContainerRef(vcr);

        this.tableData = {
            columns: [
                {
                    key: 'id',
                    name: 'ID'
                },
                {
                    key: 'name',
                    name: 'Section Name'
                },
                {
                    key: 'publisher.name',
                    name: 'Publisher'
                },
                {
                    key: 'platform',
                    name: 'Platform'
                },
                {
                    key: 'staticApiVer',
                    name: 'Script Version',
                    allowInlineEditing: true,
                    onSave: (row, key, value) => {
                        let obj = {};
                        obj[key] = value;
                        this.updateSection(row, obj)
                    }
                },
                {
                    key: 'url',
                    name: 'Website'
                },
                {
                    key: 'active',
                    name: 'Active'
                },
                {
                    key: 'createdAt',
                    name: 'Created',
                    pipe: 'date'
                }

            ],
            rows: [],
            orderBy: 'id',
            order: Constants.ORDER.DESCENDING,
            title: "Sections"
        };

        this.rowActions = [
            {
                name: 'Show',
                icon: 'remove_red_eye',
                onClick: (row) => {
                    this.viewSection(row);
                }
            },
            {
                name: 'Become',
                icon: 'open_in_new',
                onClick: (row) => {
                    this.becomeSection(row);
                }
            }];
    }

    /***
     * Returns the list of Sections
     */
    getSections():void {
        this.stateLoaderService.show();
        this.sectionService.all()
            .subscribe(
                sections => this.tableData.rows = sections,
                error => this.errorMessage = <any>error,
                () => this.stateLoaderService.hide());
    }

    /***
     * View section
     */
    viewSection(row):void {
        this.router.navigate(["/sections", row.id]);
    }

   
    /***
     * Stub
     */
    becomeSection(row):void {
        this.sectionService.become(row.id);
            // .subscribe(
            //     section => console.log(section),
            //     error => this.errorMessage = <any>error,
            //     () => this.stateLoaderService.hide());
    }

    /***
     * Filters results items for searchPhrase
     * @param items
     * @param searchPhrase
     * @returns {any}
     */
    filter(items:Array<Section>, searchPhrase:string):Array<Section> {
        return this.sectionService.filter(items, searchPhrase);
    }

    /***
     * Filters results items for searchPhrase
     * @param items
     * @param searchPhrase
     * @returns {any}
     */
    search(searchPhrase:string):Observable<Section[]> {
        return this.sectionService.search(searchPhrase);
    }

    //TODO: Add error handling here
    updateSection(row, obj):void {
        let key = Object.keys(obj)[0];
        this.stateLoaderService.show();
        this.sectionService.update(row.id, {section: obj})
            .subscribe(
                section => {
                    this.toastr.success(`Script version was updated to ${obj[key]}.`, 'Success');
                },
                error => {this.toastr.error(`Script version could not be updated`, 'Problem');},
                () => this.stateLoaderService.hide());
    }

    ngOnInit():void {
        this.loading = true;
        this.getSections();
    }

}
