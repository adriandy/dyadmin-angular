import { Adminv2Page } from './app.po';

describe('adminv2 App', () => {
  let page: Adminv2Page;

  beforeEach(() => {
    page = new Adminv2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
